<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loans';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'book_id', 'loan_date',
		'return_date'
    ];
	
	/**
     * Get the book that owns the loan.
    */
    public function book()
    {
        return $this->belongsTo('App\Book');
    }
}
