<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_data';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name','first_name','phone_number'
    ];
	
	/**
     * Get the user that owns the date user.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
