<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'authors';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'dob',
		'biography', 'photo'
    ];
	
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'biography', 'photo',
    ];
	
	/**
     * The books that belong to the author(s).
     */
    public function books()
    {
        return $this->belongsToMany('App\Book');
    }
}
