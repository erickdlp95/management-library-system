<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'books';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_id', 'genre_id', 'state_id',
		'title', 'isbn', 'cover'
    ];

	/**
     * Get the state that owns the book.
     */
    public function state()
    {
        return $this->belongsTo('App\State');
    }
	
	/**
    * The authors that belong to the book.
    */
    public function authors()
    {
        return $this->belongsToMany('App\Author');
    }
	
	/**
    * The genres that belong to the book.
    */
    public function authors()
    {
        return $this->belongsToMany('App\Genre');
    }
	
	 /**
     * Get the loans for the book.
     */
    public function loans()
    {
        return $this->hasMany('App\Loan');
    }
}
