<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('author_book')) {
			Schema::create('author_book', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->integer('author_id')->unsigned();
				$table->integer('book_id')->unsigned();
				$table->foreign('author_id')->references('id')->on('authors');
				$table->foreign('book_id')->references('id')->on('books');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('author_book');
    }
}
