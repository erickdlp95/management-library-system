<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('books')) {
			Schema::create('books', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('author_id')->unsigned();
				$table->integer('genre_id')->unsigned();
				$table->integer('state_id')->unsigned();
				$table->string('title',250);
				$table->string('isbn',250);
				$table->string('cover');
				$table->timestamps();
				
				$table->foreign('state_id')->references('id')->on('states');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
