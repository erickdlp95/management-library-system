<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('loans')) {
			Schema::create('loans', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('user_id')->unsigned();
				$table->integer('book_id')->unsigned();
				$table->date('loan_date');
				$table->date('return_date');
				$table->timestamps();
				
				$table->foreign('user_id')->references('id')->on('users');
				$table->foreign('book_id')->references('id')->on('books');
			});
		}
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loans');
    }
}
