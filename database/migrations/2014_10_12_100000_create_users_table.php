<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('users')) {
			Schema::create('users', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('username',25);
				$table->string('email',35)->unique()->nullable();
				$table->string('password');
				$table->integer('role_id')->unsigned();
				$table->string('photo');
				$table->rememberToken();
				$table->timestamps();
				$table->foreign('role_id')->references('id')->on('roles');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
