<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('states')) {
			Schema::create('states', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('name',50);
				$table->text('description');
				$table->timestamps();
			});	
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('states');
    }
}
