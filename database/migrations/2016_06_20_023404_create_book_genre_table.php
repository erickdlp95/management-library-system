<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookGenreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('book_genre')) {
			Schema::create('book_genre', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->integer('book_id')->unsigned();
				$table->integer('genre_id')->unsigned();
				$table->foreign('book_id')->references('id')->on('books');
				$table->foreign('genre_id')->references('id')->on('genres');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('book_genre');
    }
}
