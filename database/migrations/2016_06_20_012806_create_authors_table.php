<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('authors')) {
			Schema::create('authors', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('last_name',250);
				$table->string('first_name',250);
				$table->date('dob');
				$table->text('biography');
				$table->string('photo');
				$table->timestamps();
			});	
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('authors');
    }
}
